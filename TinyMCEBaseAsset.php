<?php

namespace wise5lin\tinymce5;

/*
 *          _)             __|  | _)
 * \ \  \ / | (_-<   -_) __ \  |  |    \
 *  \_/\_/ _| ___/ \___| ___/ _| _| _| _|
 *
 * @author Двуреченский Сергей
 * @link   <wise5lin@yandex.ru>
 */

use yii\web\AssetBundle;

/**
 * Класс комплекта стандартных ресурсов для редактора `TinyMCE 5`.
 */
class TinyMCEBaseAsset extends AssetBundle
{
    public $sourcePath = '@vendor/tinymce/tinymce';

    public $js = [
        'tinymce.min.js',
    ];
}

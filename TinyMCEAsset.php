<?php

namespace wise5lin\tinymce5;

/*
 *          _)             __|  | _)
 * \ \  \ / | (_-<   -_) __ \  |  |    \
 *  \_/\_/ _| ___/ \___| ___/ _| _| _| _|
 *
 * @author Двуреченский Сергей
 * @link   <wise5lin@yandex.ru>
 */

use yii\web\AssetBundle;

/**
 * Класс комплекта ресурсов для редактора `TinyMCE 5`.
 */
class TinyMCEAsset extends AssetBundle
{
    public $sourcePath = '@wise5lin/tinymce5/assets/langs';

    public $depends = [
        'wise5lin\tinymce5\TinyMCEBaseAsset',
    ];
}

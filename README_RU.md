TinyMCE 5 виджет для Yii 2 Framework
====================================

### Установка

Предпочтительный способ установки этого расширения через [composer](http://getcomposer.org/download/).

Просто запустите команду:

```
php composer.phar require --prefer-dist wise5lin/yii2-tinymce5 "~1.0.0"
```

или добавьте

```
"wise5lin/yii2-tinymce5": "~1.0.0"
```

в `require` секцию вашего `composer.json` файла.


### Использование

Как только расширение будет установлено, просто используйте его в своем коде :

```php
use wise5lin\tinymce5\TinyMCE;

<?= $form->field($model, 'content')->widget(TinyMCE::class, [
    'editorOptions' => [
         'language' => 'ru',
    ]
]) ?>
```

### Настройки

**preset** - выбор предустановленных настроек для панели редактора. Может принимать значения `basic`, `full` и `custom`.

```php
<?= $form->field($model, 'content')->widget(TinyMCE::class, [
    'preset' => 'full', // basic (по умолчанию), custom
    'editorOptions' => [
         'language' => 'ru',
    ]
]) ?>
```

`Basic preset`

![basic preset](assets/img/basic-preset.png)

`Full preset` не включает премиум плагины

![full preset](assets/img/full-preset.png)

**editorOptions** - js опции редактора, все возможные опции смотрите на [официальном сайте](https://www.tiny.cloud/docs/configure).

```php
<?= $form->field($model, 'content')->widget(TinyMCE::class, [
    'preset' => 'full',
    // https://www.tiny.cloud/docs/configure
    'editorOptions' => [
         'language' => 'ru',
    ]
]) ?>
```

**editorInlineContainerTag** - html тег контейнера редактора в режиме `inline`.

*Используется только если редактор в режиме `inline`!*

```php
<?= $form->field($model, 'content')->widget(TinyMCE::class, [
    'preset' => 'full',
    'editorOptions' => [
         'language' => 'ru',
         'inline' => true,
    ],
    'editorInlineContainerTag' => 'div' // По умолчанию
]) ?>
```

**editorInlineContainerOptions** - html атрибуты для контейнера редактора в режиме `inline`.

*Используется только если редактор в режиме `inline`!*

```php
<?= $form->field($model, 'content')->widget(TinyMCE::class, [
    'preset' => 'full',
    'editorOptions' => [
         'language' => 'ru',
         'inline' => true,
    ],
    'editorInlineContainerTag' => 'div',
    'editorInlineContainerOptions' => [
        'class' => 'my-class'
    ],
]) ?>
```

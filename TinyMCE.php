<?php

namespace wise5lin\tinymce5;

/*
 *          _)             __|  | _)
 * \ \  \ / | (_-<   -_) __ \  |  |    \
 *  \_/\_/ _| ___/ \___| ___/ _| _| _| _|
 *
 * @author Двуреченский Сергей
 * @link   <wise5lin@yandex.ru>
 */

use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\widgets\InputWidget;
use yii\base\InvalidConfigException;

/**
 * Виджет для редактора `TinyMCE 5`.
 *
 * @see https://www.tiny.cloud/features
 *
 * @example https://www.tiny.cloud/docs/demo
 *
 * ИСПОЛЬЗОВАНИЕ:
 *
 * use wise5lin\tinymce5\TinyMCE;
 *
 * <?= $form->field($model, 'content')->widget(TinyMCE::class, [
 *     'editorOptions' => [
 *          'language' => 'ru',
 *     ]
 * ]) ?>
 */
class TinyMCE extends InputWidget
{
    /**
     * Выбор предустановленных настроек для панели редактора.
     *
     * basic, full, custom
     *
     * @var string
     */
    public $preset = 'basic';
    /**
     * Js опции редактора `TinyMCE 5`, все возможные опции смотрите на официальном сайте -
     * "https://www.tiny.cloud/docs/configure".
     *
     * @var array
     */
    public $editorOptions = [];
    /**
     * HTML тег контейнера редактора в режиме `inline`.
     *
     * @var string
     */
    public $editorInlineContainerTag = 'div';
    /**
     * HTML атрибуты для контейнера редактора в режиме `inline`.
     *
     * @var array
     */
    public $editorInlineContainerOptions = [];

    /**
     * Язык редактора.
     *
     * @var string
     */
    private $language;

    //   _ \ _)   _| _|                     |       __|            |
    //   |  | |   _| _| -_)   _| -_)    \    _|    (      _ \   _` |   -_)
    //  ___/ _| _| _| \___| _| \___| _| _| \__|   \___| \___/ \__,_| \___|

    /**
     * {@inheritdoc}
     *
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();

        if (!is_array($this->editorOptions)) {
            throw new InvalidConfigException('The "editorOptions" property must be an array.');
        }

        if (!is_array($this->editorInlineContainerOptions)) {
            throw new InvalidConfigException('The "editorInlineContainerOptions" property must be an array.');
        }

        $this->id = $this->options['id'];
        $this->editorOptions['selector'] = "#{$this->id}";
        $this->editorInlineContainerOptions['id'] = $this->id;

        $this->language = ArrayHelper::getValue($this->editorOptions, 'language', 'en_US');

        switch ($this->preset) {
            case 'full':
                $preset = $this->fullPreset();
                break;
            case 'custom':
                $preset = [
                    'plugins' => [],
                    'toolbar' => [],
                ];
                break;
            default:
                $preset = $this->basicPreset();
                break;
        }

        $this->editorOptions = ArrayHelper::merge($preset, $this->editorOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $this->registerClientScript();

        if (ArrayHelper::getValue($this->editorOptions, 'inline')) {
            $content = Html::getAttributeValue($this->model, $this->attribute);

            return Html::tag($this->editorInlineContainerTag, $content, $this->editorInlineContainerOptions);
        }

        return $this->hasModel() ? Html::activeTextarea($this->model, $this->attribute, $this->options) :
            Html::textarea($this->name, $this->value, $this->options);
    }

    /**
     * Базовые настройки панели редактора.
     *
     * @return array
     */
    private function basicPreset()
    {
        return [
            'height' => 200,
            'menubar' => false,
            'plugins' => [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount',
            ],
            'toolbar' => 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify |
                bullist numlist outdent indent | removeformat | help',
        ];
    }

    /**
     * Максимальные настройки панели редактора.
     *
     * @return array
     */
    private function fullPreset()
    {
        return [
            'height' => 400,
            'menubar' => 'file edit view insert format tools table help',
            'plugins' => [
                'print preview fullpage paste importcss searchreplace autolink autosave',
                'save directionality code visualblocks visualchars fullscreen image link',
                'media template codesample table charmap hr pagebreak nonbreaking anchor toc',
                'insertdatetime advlist lists wordcount imagetools textpattern noneditable help',
                'charmap quickbars emoticons',
            ],
            'toolbar' => 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect |
                alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist |
                forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print |
                insertfile image media template link anchor codesample | ltr rtl',
            'image_advtab' => true,
            'image_caption' => true,
            'quickbars_selection_toolbar' => 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
            'contextmenu' => 'link image imagetools table',
        ];
    }

    /**
     * Регистрирует комплекты ресурсов на странице.
     */
    private function registerClientScript()
    {
        $view = $this->getView();

        $asset = TinyMCEAsset::register($view);
        $asset->js[] = "{$this->language}.min.js";

        $view->registerJs('tinymce.init('.Json::encode($this->editorOptions).');');
    }
}

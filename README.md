TinyMCE 5 widget for Yii 2 Framework
====================================

### Installation

The preferred way to install this extension is through the [composer](http://getcomposer.org/download/).

Just run the command:

```
php composer.phar require --prefer-dist wise5lin/yii2-tinymce5 "~1.0.0"
```

or add

```
"wise5lin/yii2-tinymce5": "~1.0.0"
```

into the `require` section of your `composer.json` file.


### Using

Once the extension is installed, just use it in your code:

```php
use wise5lin\tinymce5\TinyMCE;

<?= $form->field($model, 'content')->widget(TinyMCE::class) ?>
```

### Settings

**preset** - selection of predefined settings for the editor panel. It can take values `basic`, `full` and `custom`.

```php
<?= $form->field($model, 'content')->widget(TinyMCE::class, [
    'preset' => 'full', // basic (default), custom
]) ?>
```

`Basic preset`

![basic preset](assets/img/basic-preset.png)

`Full preset` does not include premium plugins

![full preset](assets/img/full-preset.png)

**editorOptions** - js editor options, see all possible options on the [official website](https://www.tiny.cloud/docs/configure).

```php
<?= $form->field($model, 'content')->widget(TinyMCE::class, [
    'preset' => 'full',
    // https://www.tiny.cloud/docs/configure
    'editorOptions' => [
         'height' => 300,
    ]
]) ?>
```

**editorInlineContainerTag** - the HTML tag for the container of the editor in `inline` mode.

*Used only if the editor is in `inline` mode!*

```php
<?= $form->field($model, 'content')->widget(TinyMCE::class, [
    'preset' => 'full',
    'editorOptions' => [
         'height' => 300,
         'inline' => true,
    ],
    'editorInlineContainerTag' => 'div' // Default
]) ?>
```

**editorInlineContainerOptions** - the HTML attributes for the container of the editor in `inline` mode.

*Used only if the editor is in `inline` mode!*

```php
<?= $form->field($model, 'content')->widget(TinyMCE::class, [
    'preset' => 'full',
    'editorOptions' => [
         'height' => 300,
         'inline' => true,
    ],
    'editorInlineContainerTag' => 'div',
    'editorInlineContainerOptions' => [
        'class' => 'my-class'
    ],
]) ?>
```
